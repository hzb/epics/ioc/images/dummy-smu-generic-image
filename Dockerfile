#
FROM registry.hzdr.de/hzb/epics/base/ubuntu_20_04:1.0.0 

# create directories
RUN mkdir -p /opt/epics/support
RUN mkdir -p /opt/epics/ioc

# configure environment
COPY RELEASE.local ${SUPPORT}/RELEASE.local
COPY RELEASE.local ${IOC}/RELEASE.local

# install autosave 
RUN git clone --depth 1 --recursive --branch R5-10-2 https://github.com/epics-modules/autosave.git ${SUPPORT}/autosave
RUN make -C ${SUPPORT}/autosave -j $(nproc)

# install seq
RUN git clone --depth 1 --recursive --branch vendor_2_2_8 https://github.com/ISISComputingGroup/EPICS-seq.git ${SUPPORT}/seq
RUN make -C ${SUPPORT}/seq -j $(nproc)

# install sscan
RUN git clone --depth 1 --recursive --branch R2-11-5 https://github.com/epics-modules/sscan.git ${SUPPORT}/sscan
RUN make -C ${SUPPORT}/sscan -j $(nproc)

# install calc
RUN git clone --depth 1 --recursive --branch R3-7-4 https://github.com/epics-modules/calc.git ${SUPPORT}/calc
RUN make -C ${SUPPORT}/calc -j $(nproc)

# install asyn
RUN git clone --depth 1 --recursive --branch R4-44-2 https://github.com/epics-modules/asyn.git ${SUPPORT}/asyn
RUN make -C ${SUPPORT}/asyn -j $(nproc)

# install pcre
RUN git clone --depth 1 --recursive --branch R8-44 https://github.com/chrschroeder/pcre.git ${SUPPORT}/pcre
RUN make -C ${SUPPORT}/pcre -j $(nproc)

# install stream
RUN git clone --depth 1 --recursive --branch 2.8.24 https://github.com/paulscherrerinstitute/StreamDevice.git ${SUPPORT}/stream
RUN make -C ${SUPPORT}/stream -j $(nproc)

#install modbus
RUN git clone --depth 1 --recursive --branch R3-2 https://github.com/epics-modules/modbus.git ${SUPPORT}/modbus
RUN make -C ${SUPPORT}/modbus -j $(nproc)

#clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install the ioc
RUN git clone --depth 1 --recursive --branch main https://codebase.helmholtz.cloud/hzb/epics/ioc/source/wagotemperatureiocsource.git ${IOC}/WagoIOC
RUN make clean -C ${IOC}/WagoIOC
RUN make -C ${IOC}/WagoIOC -j $(nproc)
RUN mkdir /opt/epics/ioc/log

